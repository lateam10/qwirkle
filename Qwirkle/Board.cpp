
#include "Board.h"
#include<set>
#include <algorithm>


Board::Board()
{
	kHeight = kInitialBoardSize;
	kWidth = kInitialBoardSize;
	m_board.resize(kHeight, std::vector<std::optional<Piece>>(kWidth));
	for (auto index = 0; index < kWidth; ++index)
	{
		m_board[index].assign(kHeight, std::nullopt);
	}
}

Board::~Board()
{
	for (auto index = 0; index < kHeight; ++index)
		m_board[index].clear();
}

size_t Board::GetHeight() const
{
	return kHeight;
}

size_t Board::GetWidth() const
{
	return kWidth;
}

std::vector<std::vector<std::optional<Piece>>> Board::GetBoard() const
{
	return m_board;
}

void Board::IncreaseBoardHeight()
{
	++kHeight;
}

void Board::IncreaseBoardWidth()
{
	++kWidth;
}
void Board::AddDownLine()
{
	std::vector<std::optional<Piece>> newLine(kWidth, std::nullopt);
	m_board.push_back(newLine);
	IncreaseBoardHeight();
}
void Board::AddRightColumn()
{
	for (auto index = 0; index < kHeight; ++index)
		m_board[index].push_back(std::nullopt);
	IncreaseBoardWidth();
}

void Board::AddUpLine()
{
	AddDownLine();

	for (int index1 = kHeight - 1; index1 >= 0; --index1)
		for (int index2 = 0; index2 < kWidth; ++index2)
		{
			if (index1 == 0)
				m_board[index1][index2] = std::nullopt;
			else
				m_board[index1][index2] = std::move(m_board[index1 - 1][index2]);
		}
}

void Board::AddLeftColumn()
{
	AddRightColumn();

	for (int index1 = 0; index1 < kHeight; ++index1)
		for (int index2 = kWidth - 1; index2 >= 0; --index2)
		{
			if (index2 == 0)
				m_board[index1][index2] = std::nullopt;
			else
				m_board[index1][index2] = std::move(m_board[index1][index2 - 1]);
		}
}

void Board::Resize(const unsigned & lineUp, const unsigned & lineDown, const unsigned & columnLeft, const unsigned & columnRight)
{
	for (int index = 0; index < lineUp; ++index)
		AddUpLine();
	for (int index = 0; index < lineDown; ++index)
		AddDownLine();
	for (int index = 0; index < columnLeft; ++index)
		AddLeftColumn();
	for (int index = 0; index < columnRight; ++index)
		AddRightColumn();

}

void Board::AddPiece(const Piece & piece, const Position & position)
{
	m_board[position.first][position.second] = piece;
}

void Board::deletePiece(const Piece & piece, const Position & position)
{
	m_board[position.first][position.second] = std::nullopt;
}

bool Board::IzolatePiece(const Position& position) const
{
	bool upPiece = true, downPiece = true, leftPiece = true, rightPiece = true;

	int line = position.first;
	int column = position.second;

	if (line != 0)
		if (m_board[line - 1][column] == std::nullopt)
			upPiece = false;

	if (line != kHeight - 1)
		if (m_board[line + 1][column] == std::nullopt)
			downPiece = false;

	if (column != 0)
		if (m_board[line][column - 1] == std::nullopt)
			leftPiece = false;

	if (column != kWidth - 1)
		if (m_board[line][column + 1] == std::nullopt)
			rightPiece = false;

	if (!IsEmpty() && upPiece == false && downPiece == false && leftPiece == false && rightPiece == false)
		return true;

	return false;
}

bool Board::IsEmpty() const
{
	for (auto index = 0; index < kHeight; ++index)
	{
		if (!(std::all_of(m_board[index].begin(), m_board[index].end(),
			[](const std::optional<Piece> & optionalPiece) { return optionalPiece == std::nullopt; })))
			return false;
	}
	return true;
}

bool Board::IsQwirkle(const Position & position) const
{
	uint32_t line = position.first;
	uint32_t column = position.second;

	int up = 1, down = 0, left = 1, right = 0;

	unsigned lineUp = line;
	while (lineUp != 0)
	{
		if (m_board[lineUp - 1][column] != std::nullopt)
		{
			++up;
			--lineUp;
		}
		else
			break;
	}
	unsigned columnLeft = column;
	while (columnLeft != 0)
	{
		if (m_board[line][columnLeft - 1] != std::nullopt)
		{
			++left;
			--columnLeft;
		}
		else
			break;
	}
	unsigned lineDown = line;
	while (lineDown != kHeight - 1)
	{
		if (m_board[lineDown + 1][column] != std::nullopt)
		{
			++left;
			++lineDown;
		}
		else
			break;
	}
	unsigned columnRight = column;
	while (columnRight != kWidth - 1)
	{
		if (m_board[line][columnRight + 1] != std::nullopt)
		{
			++left;
			++columnRight;
		}
		else
			break;
	}

	if (up + down == 6 || left + right == 6)
		return true;
	return false;
}


std::optional<Piece>& Board::operator[](const Position & position)
{
	if (position.first>= kHeight || position.second >= kWidth)
		throw " Board index out of bound.";
	return m_board[position.first][position.second];
}


unsigned Board::CalculateScore(const std::vector<Board::Position>& positions)
{
	unsigned score = 0;
	unsigned count = 0;
	bool inLine = false, inColumn = false;
	for (Position position : positions)
	{
		unsigned count = 0;
		uint32_t line = position.first;
		uint32_t column = position.second;

		unsigned lineUp = line;
		while (lineUp != 0)
		{
			if (m_board[lineUp - 1][column] != std::nullopt)
			{
				++score;
				if (lineUp==line)
						++count;
				--lineUp;
				inLine = true;
			}
			else
				break;
		}
		unsigned lineDown = line;
		while (lineDown != kHeight - 1)
		{

			if (m_board[lineDown + 1][column] != std::nullopt)
			{
				++score;
				if (lineDown==line && inLine==false)
					++count;
				++lineDown;
			}
			else
				break;
		}
		//if (line) ++score;
		unsigned columnLeft = column;
		while (columnLeft != 0)
		{
			if (m_board[line][columnLeft - 1] != std::nullopt)
			{
				++score;
				if (columnLeft==column)
					++count;
				inColumn = true;
				--columnLeft;
			}
			else
				break;
		}
		
		unsigned columnRight = column;
		while (columnRight != kWidth - 1)
		{
			if (m_board[line][columnRight + 1] != std::nullopt)
			{
				++score;
				if (columnRight==column && inColumn==false)
					++count;

				++columnRight;
			}
			else
				break;
		}
		score += count;
		// (column) ++score;
	}
	return score;
}

unsigned Board::CalculateParticularScore(const Board::Position & poz)
{
	unsigned score = 0;
	unsigned count = 0;
	bool inLine = false, inColumn = false;
	uint32_t line = poz.first;
	uint32_t column = poz.second;

	unsigned lineUp = line;
	while (lineUp != 0)
	{
		if (m_board[lineUp - 1][column] != std::nullopt)
		{
			++score;
			if (lineUp == line)
				++count;
			--lineUp;
			inLine = true;
		}
		else
			break;
	}
	unsigned lineDown = line;
	while (lineDown != kHeight - 1)
	{

		if (m_board[lineDown + 1][column] != std::nullopt)
		{
			++score;
			if (lineDown == line && inLine == false)
				++count;
			++lineDown;
		}
		else
			break;
	}
	//if (line) ++score;
	unsigned columnLeft = column;
	while (columnLeft != 0)
	{
		if (m_board[line][columnLeft - 1] != std::nullopt)
		{
			++score;
			if (columnLeft == column)
				++count;
			inColumn = true;
			--columnLeft;
		}
		else
			break;
	}

	unsigned columnRight = column;
	while (columnRight != kWidth - 1)
	{
		if (m_board[line][columnRight + 1] != std::nullopt)
		{
			++score;
			if (columnRight == column && inColumn == false)
				++count;

			++columnRight;
		}
		else
			break;
	}
	score += count;
	// (column) ++score;

return score;
}


bool Board::RightCompatibility(const Piece & piece, const Position & position) const
{
	uint32_t line = position.first;
	uint32_t column = position.second;

	if (line != 0)
		if (m_board[line - 1][column] != std::nullopt)
			if (!piece.IsCompatible(m_board[line - 1][column].value()))
			{
				throw(" Up Piece incompatible!");
				return false;
			}
	if (line > 1 )
	{
		if (m_board[line - 2][column] != std::nullopt)
			if (!piece.IsCompatible(m_board[line - 2][column].value()))
			{
				throw(" Up Pieces incompatibles!");
				return false;
			}
	}
	if (line != kHeight - 1)
		if (m_board[line + 1][column] != std::nullopt)
			if (!piece.IsCompatible(m_board[line + 1][column].value()))
			{
				throw(" Down Piece incompatible!");
				return false;
			}
	if (line < kHeight-2)
		if (m_board[line + 2][column] != std::nullopt)
			if (!piece.IsCompatible(m_board[line + 2][column].value()))
			{
				throw(" Down Pieces incompatibles!");
				return false;
			}
	if (column != 0)
		if (m_board[line][column - 1] != std::nullopt)
			if (!piece.IsCompatible(m_board[line][column - 1].value()))
			{
				throw(" Left Piece incompatible!");
				return false;
			}
	if (column >1 )
		if (m_board[line][column - 2] != std::nullopt)
			if (!piece.IsCompatible(m_board[line][column - 2].value()))
			{
				throw(" Left Pieces incompatibles!");
				return false;
			}
	if (column != kWidth - 1)
		if (m_board[line][column + 1])
			if (!piece.IsCompatible(m_board[line][column + 1].value()))
			{
				throw(" Right Piece incompatible!");
				return false;
			}
	if (column < kWidth - 2)
		if (m_board[line][column + 2])
			if (!piece.IsCompatible(m_board[line][column + 2].value()))
			{
				throw(" Right Pieces incompatibles!");
				return false;
			}
	if (DuplicatePiece(piece, position))
	{
		throw(" Duplicate Piece! You tried to complete a Qwirkle Line or Column with an existing Piece! ");
		return false;
	}

	return true;
}

bool Board::DuplicatePiece(const Piece & piece, const Position & position) const
{
	uint32_t line = position.first;
	uint32_t column = position.second;

	unsigned lineUp = line;
	while (lineUp != 0)
	{
		if (m_board[lineUp - 1][column] == std::nullopt)
			break;
		if (m_board[lineUp - 1][column].value() == piece)
			return true;
		--lineUp;
	}

	unsigned columnLeft = column;
	while (columnLeft != 0)
	{
		if (m_board[line][columnLeft - 1] == std::nullopt)
			break;
		if (m_board[line][columnLeft - 1].value() == piece)
			return true;
		--columnLeft;
	}

	unsigned lineDown = line;
	while (lineDown != kHeight - 1)
	{
		if (m_board[lineDown + 1][column] == std::nullopt)
			break;
		if (m_board[lineDown + 1][column].value() == piece)
			return true;
		++lineDown;
	}

	unsigned columnRight = column;
	while (columnRight != kWidth - 1)
	{
		if (m_board[line][columnRight + 1] == std::nullopt)
			break;
		if (m_board[line][columnRight + 1].value() == piece)
			return true;
		++columnRight;
	}
	return false;
}

bool Board::RightChoice(const Board::Position & position, const Piece & piece) const
{
	if (position.first < 0)
	{
		throw(" Incorrect Line Position! ");
		return false;
	}

	if (position.second < 0)
	{
		throw(" Incorrect Column Position! ");
		return false;
	}
	if (position.first > kHeight - 1)
	{
		throw(" Incorrect Line Position! ");
		return false;
	}
	if (position.second > kWidth - 1)
	{
		throw(" Incorrect Column Position! ");
		return false;
	}

	if (m_board[position.first][position.second] != std::nullopt)
	{
		throw(" Position is occupied by another Piece! ");
		return false;
	}

	if (IzolatePiece(position))
	{
		throw(" You tried to put an Izolate Piece! You should place your Piece next to another with same attribut! ");
		return false;
	}

	try
	{
		RightCompatibility(piece, position);
	}
	catch (const char* errorMessage)
	{
		throw  errorMessage;
		return false;
	}

	return true;
}

std::ostream & operator<<(std::ostream & os, const Board & board)
{
	os << "     __";
	for (int index = 0; index < board.kWidth; ++index)
		os << index + 1 << "___";
	std::cout << std::endl;

	for (int index1 = 0; index1 < board.m_board.size(); ++index1)
	{
		if (index1 < 9)
			os << " " << index1 + 1 << designPattern1;
		else
			os << " " << index1 + 1 << designPattern2;
		for (int index2 = 0; index2 < board.m_board[index1].size(); ++index2)
		{
			if (board.m_board[index1][index2] == std::nullopt)
				os << kEmptyBoardCell;
			else
				os << board.m_board[index1][index2].value();
		}
		os << std::endl;
	}
	os << std::endl << std::endl;
	return os;
}
