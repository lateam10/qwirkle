#include "BoardPieces.h"

BoardPieces::BoardPieces(const Board & board)
{
	for (unsigned index1 = 0; index1 < board.GetHeight(); ++index1)
		for (unsigned index2 = 0; index2 < board.GetWidth(); ++index2)
		{
			auto element = board.GetBoard()[index1][index2];
			if (element!= std::nullopt)
				addPiece(element.value(), std::make_pair(index1, index2));
		}
}

std::vector<std::pair<Piece, Board::Position>> BoardPieces::GetPieces() const
{
	return boardPieces;
}

void BoardPieces::addPiece(const Piece & piece, const Board::Position & position)
{
	std::pair<Piece, Board::Position> pairTo = std::make_pair(piece, position);
	boardPieces.push_back(pairTo);
}

void BoardPieces::modifyPositions(unsigned & line, unsigned & column)
{
	for (int i = 0; i < boardPieces.size(); ++i)
	{
		boardPieces[i].second.first += line;
		boardPieces[i].second.second += column;
	}
}

std::vector<std::pair<Piece, Board::Position>> BoardPieces::findCompatiblePieces(const Piece & piece)
{
	std::vector<std::pair<Piece, Board::Position>> compatiblesPieces;
	for (unsigned index = 0; index < boardPieces.size(); ++index)
		if (piece.IsCompatible(boardPieces[index].first))
			compatiblesPieces.push_back(boardPieces[index]);
	return compatiblesPieces;
}

std::vector<Board::Position> BoardPieces::AvaiablePositions(const Board & board, const Piece & piece, const Board::Position& position)
{
	std::vector<Board::Position> positions;
	unsigned line = position.first;
	unsigned column = position.second ;
	std::vector<std::vector<std::optional<Piece>>> currentBoard = board.GetBoard();
	if(line>0)
		if (!currentBoard[line - 1][column ].has_value()) 
		{
			Board::Position poz = std::make_pair(line - 1, column);
			positions.push_back(poz);
		}
		
	if(line<board.GetHeight()-1)
		if (!currentBoard[line + 1][column ].has_value())
		{
			Board::Position poz = std::make_pair(line +1, column );
			positions.push_back(poz);
		}
	if (column>0)
		if (!currentBoard[line ][column-1].has_value())
		{
			Board::Position poz = std::make_pair(line , column - 1);
			positions.push_back(poz);
		}
	if (column < board.GetWidth()-1)
		if (!currentBoard[line][column + 1].has_value())
		{
			Board::Position poz = std::make_pair(line , column + 1);
			positions.push_back(poz);
		}
	return positions;

}

std::optional<Piece> BoardPieces::CheckUpPosition(const Board & board, const Piece & piece, const Board::Position & position)
{
	unsigned line = position.first;
	unsigned column = position.second;
	if (line > 0)
		return board.GetBoard()[line - 1][column];
}

std::optional<Piece> BoardPieces::CheckDownPosition(const Board & board, const Piece & piece, const Board::Position & position)
{
	unsigned line = position.first;
	unsigned column = position.second;
	if (line < board.GetHeight() - 1)
		return board.GetBoard()[line + 1][column];
}

std::optional<Piece> BoardPieces::CheckLeftPosition(const Board & board, const Piece & piece, const Board::Position & position)
{
	unsigned line = position.first;
	unsigned column = position.second;
	if (column > 0)
		return board.GetBoard()[line][column - 1];
}

std::optional<Piece> BoardPieces::CheckRightPosition(const Board & board, const Piece & piece, const Board::Position & position)
{
	unsigned line = position.first;
	unsigned column = position.second;
	if (column < board.GetWidth() - 1)
		return board.GetBoard()[line][column + 1];
}

std::vector<Board::Position> BoardPieces::CheckPositions(const Board & board, const Piece & piece, const Board::Position & position)
{
	std::vector<Board::Position> positions;
	unsigned line = position.first;
	unsigned column = position.second;
	if (!CheckUpPosition(board, piece, position).has_value())
		positions.push_back(std::make_pair(line - 1, column));
	if (!CheckDownPosition(board, piece, position).has_value())
		positions.push_back(std::make_pair(line + 1, column));
	if (!CheckLeftPosition(board, piece, position).has_value())
		positions.push_back(std::make_pair(line , column - 1));
	if (!CheckRightPosition(board, piece, position).has_value())
		positions.push_back(std::make_pair(line, column + 1));
	return positions;
}
