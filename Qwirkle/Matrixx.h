#pragma once

#include<iostream>
#include<vector>

template<typename Type>
class Matrix
{
public:
	using Position = std::pair<uint32_t, uint32_t>;

public:
	std::size_t kHeight;
	std::size_t kWidth;
	std::vector<std::vector<Type>> matrix;

public:
	Matrix();
	~Matrix();

public:

	void increaseBoardHeight();
	void increaseBoardWidth();

public:
	void addDownLine();
	void addRightColumn();
	void addUpLine();
	void addLeftColumn();
	void resize(const unsigned & lineUp, const unsigned & lineDown, const unsigned& columnLeft, const unsigned  &  columnRight);

public:
	//template<typename Type>
	//friend std::ostream & operator << (std::ostream & os, const Matrix<Type> & matrix);

};

template<typename Type>
Matrix<Type>::Matrix() : kHeight(kInitialBoardSize), kWidth(kInitialBoardSize)
{
	matrix.resize(kInitialBoardSize, std::vector<Type>(kInitialBoardSize));
	for (auto it = 0; it < kInitialBoardSize; ++it)
		matrix[it].assign(kInitialBoardSize, kEmptyBoardCell);
}

template<typename Type>
Matrix<Type>::~Matrix()
{
	for (auto it = 0; it < kHeight; ++it) 
		matrix[it].clear();
}

template<typename Type>
inline void Matrix<Type>::increaseBoardHeight()
{
	++kHeight;
}

template<typename Type>
void Matrix<Type>::increaseBoardWidth()
{
	++kWidth;
}

template<typename Type>
inline void Matrix<Type>::addDownLine()
{
	std::vector<int> newLine(kWidth, kEmptyBoardCell);
	matrix.push_back(newLine);
	increaseBoardHeight();
}

template<typename Type>
inline void Matrix<Type>::addRightColumn()
{
	for (auto i = 0; i < kHeight; ++i)  
		matrix[i].push_back(kEmptyBoardCell);
	increaseBoardWidth();
}

template<typename Type>
void Matrix<Type>::addUpLine()
{
	addDownLine();

	for (int i = kHeight - 1; i >= 0; --i)
		for (int j = 0; j < kWidth; ++j)
		{
			if (i == 0)
				matrix[i][j] = kEmptyBoardCell;
			else
				matrix[i][j] = std::move(matrix[i - 1][j]);
		}
}

template<typename Type>
void Matrix<Type>::addLeftColumn()
{
	addRightColumn();

	for (int i = 0; i < kHeight; ++i)
		for (int j = kWidth - 1; j >= 0; --j)
		{
			if (j == 0)
				matrix[i][j] = kEmptyBoardCell;
			else
				matrix[i][j] = std::move(matrix[i][j - 1]);
		}
}
template<typename Type>
void Matrix<Type>::resize(const unsigned & lineUp, const unsigned & lineDown, const unsigned & columnLeft, const unsigned & columnRight)
{
	for (auto i = 0; i < lineUp; ++i)
		addUpLine();
	for (auto i = 0; i < lineDown; ++i)
		addDownLine();
	for (auto i = 0; i < columnLeft; ++i)
		addLeftColumn();
	for (auto i = 0; i < columnRight; ++i)
		addRightColumn();
}
//template<typename Type>
//std::ostream & operator << (std::ostream & os, const Matrix<Type> & myMatrix)
//{
//	os << "     __";
//	for (auto i = 0; i < myMatrix.kWidth; ++i)
//		os << i + 1 << "___";
//	std::cout << std::endl;
//
//	for (int i = 0; i < myMatrix.matrix.size(); ++i)
//	{
//		//(i < 9) ? (os << " " << i + 1 << designPattern1 ) : ( os << " " << i + 1 << designPattern2);
//		if (i < 9)
//			os << " " << i + 1 << designPattern1;
//		else
//			os << " " << i + 1 << designPattern2;
//		for (auto j = 0; j <myMatrix.matrix[i].size(); ++j)
//		{
//			if (myMatrix.matrix[i][j] == std::nullopt)
//				os << kEmptyBoardCell;
//			else
//				os << myMatrix.matrix[i][j].value();
//		}
//		os << std::endl;
//	}
//	os << std::endl << std::endl;
//	return os;
//}
	
