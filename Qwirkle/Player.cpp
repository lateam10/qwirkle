#include "Player.h"
#include"Board.h"
#include<random>
#include<string>
#include<fstream>
#include<set>
const int kInvalidPiecePosition = -1;

Player::Player(const std::string & name) : m_name(name)
{
	m_score = 0;
	m_currentPieces.reserve(kInitialBoardSize);
	m_hints = 5;
}

Player::~Player()
{
	m_currentPieces.clear();
}

const std::string & Player::GetName() const
{
	return m_name;
}

const unsigned int Player::GetScore() const
{
	return m_score;
}

std::vector<Piece> Player::GetCurrentPieces() const
{
	return m_currentPieces;
}

uint16_t Player::GetHints() const
{
	return m_hints;
}

void Player::Turn(UnusedPieces & unusedPieces, Board & board, unsigned int opponentScore)
{
	std::ofstream of("syslog.log", std::ios::app);
	Logger logger(of);

	std::vector<Board::Position> positions;
	uint16_t option, numberOfPieces;
	Piece piece;
	Board::Position position;
	unsigned int playerMoves, line, column;
	unsigned int lineUp = 0, lineDown = 0, columnLeft = 0, columnRight = 0;
	std::cout << "           " << m_name << " is your Turn! (-_-) " << std::endl;
	ShowCurrentPieces();

	std::cout << " The current Board is:" << std::endl;
	std::cout << board;

	std::cout << " Your options are: " << std::endl;
	std::cout << " 1. To change one or more pieces" << std::endl;
	std::cout << " 2. To put Pieces on the Board" << std::endl;
	std::cout << " Enter the number of your option: ";
	std::cin >> option;

	switch (option)
	{
	case 1:
	{
		std::cout << " How many pieces do you want to Change?";
		std::cin >> numberOfPieces;
		logger.verify(numberOfPieces);
		if (numberOfPieces == 6)
		{
			ChangeAllPieces(unusedPieces);
			std::cout << " After this Turn, your Pieces are: " << std::endl;
			ShowCurrentPieces();
			std::cout << " Pieces unused: " << unusedPieces.GetUnusedPieces().size() << std::endl;
			logger.log("The player changed all pieces. ", Logger::Level::Info);
		}
		else
		{
			std::cout << " Choose the pieces you want to change:" << std::endl;
			std::vector<Piece> pieces;
			Piece pieceToChange;
			bool correctPiece = false;
			for (auto i = 0; i < numberOfPieces; ++i)
			{
				std::cin >> pieceToChange;
				while (correctPiece == false)
				{
					try
					{
						correctPiece = PickedCorrectPiece(pieceToChange);
					}
					catch (const char* errorMessage)
					{
						std::cout << errorMessage << std::endl;
						std::cout << " Let's Try Again! Pay attention and choose another Piece :) " << std::endl;
						std::cout << " Color + Shape:" << std::endl;
						std::cin >> pieceToChange;
						logger.log("Incorrect piece to change.", Logger::Level::Error);
					}
				}
				pieces.push_back(pieceToChange);
			}
			ChangeSomePieces(pieces, unusedPieces);
			std::cout << " After this Turn, your Pieces are : " << std::endl;
			ShowCurrentPieces();

			logger.log(" The player changed a number of pieces. ", Logger::Level::Info);
			std::cout << " Pieces unused: " << unusedPieces.GetUnusedPieces().size() << std::endl;
		}
		break;
	}

	case 2:
	{
		std::cout << " How many pieces do you want to place to the Board? " << std::endl;
		std::cin >> playerMoves;
		logger.verify(playerMoves);

		for (auto index = 0; index < playerMoves; ++index)
		{
			PickPiece(piece);
			bool insert = true;
			std::string response;

			if (this->GetScore() < opponentScore)
			{
				std::cout << " Your score is lower than your opponent. Do you want to use one of the " << this->GetHints() << " hints?" << std::endl;
				std::cout << "Enter -> 'y' (YES) " << std::endl;
				std::cout << "      -> 'n' (NO) " << std::endl;
				std::cin >> response;
				if (response == "y")
				{
					unsigned max = 0;
					Board::Position maxPosition;
					Piece goodPiece;
					//UseOneHint();
					this->m_hints--;
					BoardPieces boardPieces(board);
					for (int i = 0; i < boardPieces.GetPieces().size(); ++i)
						std::cout << boardPieces.GetPieces()[i].first << " " << boardPieces.GetPieces()[i].second.first + 1 << boardPieces.GetPieces()[i].second.second + 1 << std::endl;
					std::vector<std::pair<Piece, Board::Position>> compatiblesPiecesPoz = boardPieces.findCompatiblePieces(piece);
					//std::cout << std::endl << "Compatibles pieces: " << std::endl;

					//AFISARE PIESE COMPATIBILE+POZITII
					/*for (auto compatiblePieces : compatiblesPiecesPoz)
						std::cout << compatiblePieces.first << " " << compatiblePieces.second.first << compatiblePieces.second.second;*/
					
					//PT FIECARE PIESA COMPATIBILA+POZ
					for (auto compatPiece : compatiblesPiecesPoz)
					{
						std::vector<Board::Position>  foundPositions = boardPieces.CheckPositions(board, compatPiece.first, compatPiece.second);
					/*	std::cout << "Posible positions:";
						for (auto poz : foundPositions)
							std::cout << poz.first+1 << poz.second+1 << std::endl;*/

						for (auto element : foundPositions)
						{
							board.AddPiece(compatPiece.first, element);
							unsigned currentScore = (board.CalculateParticularScore(element));
							if (currentScore > max)
							{
								max = currentScore;
								maxPosition = element;
								goodPiece = compatPiece.first;
							}
							board.deletePiece(compatPiece.first, element);
						}
					}
					
					//else if (response == "n")
						///break;
					std::cout << "Piece " << goodPiece <<" "<< maxPosition.first+1 << maxPosition.second + 1;
					position.first = maxPosition.first;
					position.second = maxPosition.second;
					piece = goodPiece;
					board.AddPiece(piece, position);
					if (board.IsQwirkle(position))
					{
						std::cout << "QWIRKLE!" << std::endl;
						IncreaseScore(6);
						logger.log("QWIRKLE!", Logger::Level::Warning);
					}
					AfterPlacingPiece(piece, board);
				}
				else
					PlacePiece(piece, board, unusedPieces, position);
			}
			else
				PlacePiece(piece, board, unusedPieces, position);
			for (auto currentPosition : positions)
					if (position.first == currentPosition.first || position.second == currentPosition.second)
					{
						insert = false;
						break;
					}
				if (insert)
					positions.push_back(position);

				if (position.first < board.GetHeight() / 2)
					lineUp++;

				if (position.first < board.GetHeight() / 2)
					lineDown++;

				if (position.second < board.GetWidth() / 2)
					columnLeft++;

				if (position.second > board.GetWidth() / 2)
					columnRight++;

				std::cout << std::endl;


			}
			GeneratePieces(playerMoves, unusedPieces);
			std::cout << " After this Turn, your pieces are: " << std::endl;
			ShowCurrentPieces();
			std::cout << " After this Turn, your Score is: ";

			//boardPieces.addPiece(piece, position);
			IncreaseScore(board.CalculateScore(positions));
			std::cout << m_score << std::endl;
			positions.clear();
			logger.log("The player placed pieces on board.", Logger::Level::Info);
			board.Resize(lineUp, lineDown, columnLeft, columnRight);
			std::cout << std::endl << "Pieces unused: " << unusedPieces.GetUnusedPieces().size() << std::endl;

		}
		break;
	}
	}



void Player::PickPiece(Piece & piece)
{
	std::ofstream of("syslog.log", std::ios::app);
	Logger logger(of);

	ShowCurrentPieces();

	std::cin >> piece;
	std::cout << " You want to place the piece: " << piece << std::endl;
	bool correct = false;
	while (!correct)
	{
		try
		{
			correct = PickedCorrectPiece(piece);
		}
		catch (const char* errorMessage)
		{
			std::cout << errorMessage << std::endl;
			std::cout << " Let's Try Again! Pay attention and choose another Piece! " << std::endl;
			std::cin >> piece;
			std::cout << " You want to place the piece: " << piece << std::endl;
			logger.log("Incorrect piece! ", Logger::Level::Error);
		}
	}
}

void Player::PlacePiece(Piece & piece, Board & board, UnusedPieces & unusedPieces, Board::Position& position)
{
	std::ofstream of("syslog.log", std::ios::app);
	Logger logger(of);

	getPosition(position,piece);
	bool rightPosition = false;

	while (!rightPosition)
	{
		try
		{
			rightPosition = board.RightChoice(position, piece);
		}
		catch (const char* errorMessage)
		{
			std::cout << errorMessage << std::endl;

			std::string option;
			std::cout << " Do you want to quit? Press YES or NO (-_-)" << std::endl;
			std::cin >> option;
			if (option == "YES")
			{
				break;
				rightPosition = false;
			}
			else
			{
				insertAnotherPosition(position);
				logger.log("Wrong position!", Logger::Level::Error);
			}
		}

	}

	if (rightPosition)
	{
		board.AddPiece(piece, position);
	}

	if (board.IsQwirkle(position))
	{
		std::cout << "QWIRKLE!" << std::endl;
		IncreaseScore(6);
		logger.log("QWIRKLE!", Logger::Level::Warning);
	}
	AfterPlacingPiece(piece, board);
}

void Player::DeletePickedPiece(const Piece & piece)
{
	m_currentPieces.erase(m_currentPieces.begin() + FindPiecePosition(piece));
}

void Player::IncreaseScore(const unsigned & newScore)
{
	m_score += newScore;
}

int Player::FindPiecePosition(const Piece & piece)
{
	for (auto position = 0; position < m_currentPieces.size(); ++position)
		if (m_currentPieces[position] == piece)
			return position;
	return kInvalidPiecePosition;
}

void Player::GenerateCurrentPieces(UnusedPieces &Pieces)
{
	std::random_device random_device;
	std::mt19937 engine{ random_device() };

	for (auto index = 0; index < kInitialBoardSize; ++index)
	{
		std::uniform_int_distribution<unsigned> dist(0, Pieces.GetUnusedPieces().size() - 1);
		Piece randomPiece = Pieces.GetUnusedPieces()[dist(engine)];
		m_currentPieces.push_back(randomPiece);
		Pieces.DeletePiece(randomPiece);
	}
}

void Player::ChangeAllPieces(UnusedPieces & unusedPieces)
{
	for (auto piece : m_currentPieces)
		unusedPieces.AddPiece(piece);
	m_currentPieces.clear();
	GenerateCurrentPieces(unusedPieces);
}

void Player::ChangeSomePieces(std::vector<Piece> & pieces, UnusedPieces & unusedPieces)
{
	for (auto piece : pieces)
		unusedPieces.AddPiece(piece);
	DeletePieces(pieces);
	GeneratePieces(pieces.size(), unusedPieces);
	pieces.clear();
}

void Player::insertAnotherPosition(Board::Position & position)
{
	unsigned line, column;
	std::cout << " Please insert another position!" << std::endl;
	std::cout << " Line : "; std::cin >> line;
	std::cout << " Column : "; std::cin >> column;
	position.first = line - 1;
	position.second = column - 1;
}

void Player::getPosition(Board::Position & position, const Piece & piece)
{
	unsigned int line, column;

	std::cout << " " << m_name << ", please select the location for Piece " << piece << std::endl;
	std::cout << " Line: "; std::cin >> line;
	std::cout << " Column: "; std::cin >> column;
	position.first = line - 1;
	position.second = column - 1;
}

void Player::AfterPlacingPiece(const Piece & piece, const Board & board)
{
	DeletePickedPiece(piece);
	std::cout << std::endl;
	ShowCurrentPieces();
	std::cout << board;
}

void Player::DeletePieces(std::vector<Piece> & pieces)
{
	for (auto piece : pieces)
		DeletePickedPiece(piece);
}

void Player::ShowCurrentPieces()
{
	std::cout << m_name << ", your Current Pieces are: " << std::endl;
	for_each(m_currentPieces.begin(), m_currentPieces.end(), [](const Piece & piece) {std::cout << "   " << piece << std::endl; });
	std::cout << std::endl;
}


void Player::GeneratePieces(const int & number, UnusedPieces & unusedPieces)
{
	std::random_device random_device;
	std::mt19937 engine{ random_device() };

	for (auto index = 0; index < number; ++index)
	{
		std::uniform_int_distribution<unsigned> dist(0, unusedPieces.GetUnusedPieces().size() - 1);
		Piece randomPiece = unusedPieces.GetUnusedPieces()[dist(engine)];
		m_currentPieces.push_back(randomPiece);
		unusedPieces.DeletePiece(randomPiece);
	}
}

bool Player::PickedCorrectPiece(const Piece & piece) const
{
	int color = static_cast<int>(piece.GetColor()) - 1;
	int shape = static_cast<int>(piece.GetShape()) - 1;
	if (color < 0 || color> 5)
	{
		throw (" You inserted wrong Color attribute for the Piece! ");
		return false;
	}
	if (shape < 0 || shape >5)
	{
		throw (" You inserted wrong Shape attribute for the Piece! ");
		return false;
	}

	for (Piece currentPiece : m_currentPieces)
		if (currentPiece == piece)
			return true;
	throw(" You selected a piece that doesn't exist in your collection. :( ");
	return false;
}
