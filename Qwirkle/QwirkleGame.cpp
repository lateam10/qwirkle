#include "QwirkleGame.h"
#include"Piece.h"
#include"UnusedPieces.h"
#include"Board.h"
#include"Player.h"
#include"BoardPieces.h"
#include<exception>
#include<string>
#include<fstream>

void QwirkleGame::Run()
{
	std::ofstream of("syslog.log", std::ios::app);
	Logger logger(of);
	logger.log(" Started Game!", Logger::Level::Info);

	Board board;
	UnusedPieces unusedPieces;
	Player player1, player2;
	

	logger.log("Created board and 108 unused pieces.", Logger::Level::Info);

	std::string playerName;
	std::cout << " First player name : ";
	std::cin >> playerName;
	std::cout << " Hi, " << playerName << " (-_-)" << std::endl;
	Player firstPlayer(playerName);
	firstPlayer.GenerateCurrentPieces(unusedPieces);

	std::cout << " Second player name : ";
	std::cin >> playerName;
	std::cout << " Hi, " << playerName << " (-_-)" << std::endl<<std::endl;
	Player secondPlayer(playerName);
	secondPlayer.GenerateCurrentPieces(unusedPieces);

	Clear();

	logger.log("Created players.", Logger::Level::Info);
	Functions::introduction();
	std::cout << "  This is the initial board:" << std::endl;
	std::cout << board;

	std::cout << "                        Good Luck " << firstPlayer.GetName() << " and " << secondPlayer.GetName()
		<< " ! (-_-) " << std::endl << std::endl << std::endl;
	Clear();

	while (true)
	{
		std::cout << std::endl<<std::endl
			<<"  # As a first Turn, we should decide who starts the Game." 
			<< std::endl
			<< "  # The number of the pieces you can put on the Board will decide the first Player" 
			<< std::endl;

		logger.log(" Some information about Qwirkle.", Logger::Level::Info);

		Clear();

		unsigned player1Moves, player2Moves;
		ChooseNumberOfPieces(firstPlayer, player1Moves);
		logger.verify(player1Moves);
		Clear();


		ChooseNumberOfPieces(secondPlayer, player2Moves);
		logger.verify(player2Moves);
		Clear();

		std::reference_wrapper<Player> startingPlayer = firstPlayer;
		std::reference_wrapper<Player> nextPlayer = secondPlayer;
		Piece piece;

		if (player1Moves < player2Moves)
			std::swap(startingPlayer, nextPlayer);

		logger.log("Decided who starts the game.", Logger::Level::Info);
		logger.log("The rounds strated!", Logger::Level::Warning);


		while (unusedPieces.GetUnusedPieces().size() != 0)
		{

			startingPlayer.get().Turn(unusedPieces, board, nextPlayer.get().GetScore());
			Clear();


			nextPlayer.get().Turn(unusedPieces, board, startingPlayer.get().GetScore());
			Clear();

			std::swap(startingPlayer, nextPlayer);
		}

		if (player1.GetScore() > player2.GetScore())
			std::cout << " Congrulations!" << std::endl <<
			player1.GetName() << ", you won the game! (-_-)";
		else
			if (player2.GetScore() > player1.GetScore())
				std::cout << " Congrulations!" << std::endl <<player2.GetName() << ", you won the game! (-_-)";
			else
				std::cout << " WOW, you have the same score!" <<std::endl << "Congrulations, " << player1.GetName() <<
				" and " << player2.GetName() << "! (-_-)";
	}
	Clear();
	logger.log(" The game is over.", Logger::Level::Info);
}

void QwirkleGame::ChooseNumberOfPieces(Player & player, unsigned int & playerMoves)
{
	player.ShowCurrentPieces();
	std::cout <<" "<< player.GetName() << ", how many pieces do you want to place on the Board? " << std::endl;
	std::cin >> playerMoves;
}

void QwirkleGame::Clear()
{
	system("pause");
	system("cls");
}



