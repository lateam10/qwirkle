#pragma once
#include<iostream>

template<class T>
class SmartPointer
{

public:
	explicit SmartPointer(T *p = NULL);
	~SmartPointer();
	
public:
	T & operator *();
	T * operator -> ();

private:
	T *pointer;

};
