#pragma once

#ifdef QWIRKLEDLL_EXPORTS
#define QWIRKLEDLL_API __declspec(dllexport)
#else
#define QWIRKLEDLL_API __declspec(dllimport)
#endif 

#include <iostream>
#include <string>
#include<vector>
#include<exception>
#include<fstream>


class QWIRKLEDLL_API Functions
{
public:

	static void introduction();

};


class QWIRKLEDLL_API Logger
{
public:
	enum class Level
	{
		Info,
		Warning,
		Error
	};

public:
	Logger(std::ostream& os, Level minimumLevel = Level::Info);

	void log(const char * message, Level level);
	void log(const std::string& message, Level level);

	void setMinimumLogLevel(Level level);

	void verify(int a);
	void verifyOption(int b);

private:
	std::ostream& os;
	Level minimumLevel;
};