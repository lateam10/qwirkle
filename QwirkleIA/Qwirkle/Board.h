#pragma once
#include<iostream>
#include<vector>
#include"Piece.h"
#include<optional>

const char kEmptyBoardCell[] = " __ ";
const unsigned kInitialBoardSize = 6;
const char designPattern1[] = "  |";
const char designPattern2[] = " |";

class Board
{
public:
	using Position = std::pair<unsigned, unsigned>;

public:
	Board();
	~Board();
public:
	size_t GetHeight() const;
	size_t GetWidth() const;
	std::vector<std::vector<std::optional<Piece> > > GetBoard() const;
public:
	void IncreaseBoardHeight();
	void IncreaseBoardWidth();
	void Resize(const unsigned & lineUp, const unsigned & lineDown, const unsigned & columnLeft, const unsigned & columnRight);
	void SetResize(const Position & position, unsigned&  lineUp, unsigned& lineDown, unsigned& columnRight, unsigned& columnLeft);
public:
	void AddDownLine();
	void AddRightColumn();
	void AddUpLine();
	void AddLeftColumn();
public:
	void AddPiece(const Piece & piece, const Position & position);
	void DeletePiece(const Piece & piece, const Position & position);
	bool IsEmpty() const;
	bool IsQwirkle(const Position & position) const;
	std::optional<Piece> & operator[] (const Position& position);
public:
	unsigned CalculateScore(const std::vector <Board::Position> & positions);
	unsigned CalculateParticularScore(const Board::Position& poz);
public:
	bool RightChoice(const Board::Position & position, const Piece & piece) const;
	bool RightCompatibility(const Piece & piece, const Position & position) const;
	bool DuplicatePiece(const Piece & piece, const Position & position) const;
	bool IzolatePiece(const Position & position) const;
public:
	friend std::ostream & operator << (std::ostream & os, const Board & board);
private:
	size_t kWidth;
	size_t kHeight;
	std::vector<std::vector<std::optional<Piece>>> m_board;

};