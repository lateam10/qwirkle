#pragma once
#include"Piece.h"
#include"Board.h"
#include<vector>

class BoardPieces
{
public:
	BoardPieces(const Board & board);
public:
	std::vector<std::pair<Piece, Board::Position>> GetPieces() const;
	void AddPiece(const Piece & piece, const Board::Position & position);
public:
	std::vector<std::pair<Piece, Board::Position>> FindCompatiblePiecesWithPositions(const Piece & piece);
	std::vector<Board::Position> AvaiablePositions(const Board & board, const Piece & piece, const Board::Position & position);
public:
	std::optional<Piece> CheckUpPosition(const Board & board, const Piece & piece, const Board::Position & position);
	std::optional<Piece> CheckDownPosition(const Board & board, const Piece & piece, const Board::Position & position);
	std::optional<Piece> CheckLeftPosition(const Board & board, const Piece & piece, const Board::Position & position);
	std::optional<Piece> CheckRightPosition(const Board & board, const Piece & piece, const Board::Position & position);
	std::vector<Board::Position> CheckPositions(const Board & board, const Piece & piece, const Board::Position & position);
private:
	std::vector<std::pair<Piece, Board::Position>> boardPieces;
};

