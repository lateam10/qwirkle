#include "Piece.h"
#include"Board.h"

Piece::Piece() :
	Piece(Color::None, Shape::None)
{

}

Piece::Piece(Color color, Shape shape) :
	m_color(color),
	m_shape(shape)
{

}

Piece::Piece(const Piece& other)
{
	*this = other;
}

Piece::Piece(Piece&& other)
{
	*this = std::move(other);
}

Piece::~Piece()
{
	m_color = Color::None;
	m_shape = Shape::None;
}
Piece::Color Piece::GetColor() const
{
	return m_color;
}

Piece::Shape Piece::GetShape() const
{
	return m_shape;
}

bool Piece::IsCompatible(const Piece& piece) const
{
	Color color = piece.GetColor();
	Shape shape = piece.GetShape();
	if (shape == m_shape && color == m_color) return false;
	if (shape == m_shape && color != m_color) return true;
	if (color == m_color && shape != m_shape) return true;
	return false;
}

Piece & Piece::operator= (const Piece & other)
{
	m_color = other.m_color;
	m_shape = other.m_shape;

	return *this;
}

Piece & Piece::operator= (Piece && other)
{
	m_color = other.m_color;
	m_shape = other.m_shape;

	new(&other) Piece;
	return *this;
}

bool Piece::operator==(const Piece & other) const
{
	if (this->m_color == other.m_color && this->m_shape == other.m_shape)
		return true;
	return false;
}


std::istream & operator >> (std::istream & is, Piece & piece)
{

	std::cout << std::endl << " Enter the attributes of the Piece:" << std::endl;
	std::cout << "Color + Shape:" << std::endl;
	uint8_t color, shape;
	if (is >> color >> shape)
	{
		piece.m_color = static_cast<Piece::Color>(color + 1);
		piece.m_shape = static_cast<Piece::Shape>(shape + 1);
	}
	return is;
}

std::ostream & operator << (std::ostream& os, const Piece& piece)
{
	return os << " " <<
		static_cast<int>(piece.m_color) - 1 <<
		static_cast<int>(piece.m_shape) - 1 << " ";
}