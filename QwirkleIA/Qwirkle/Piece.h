#pragma once
#include <iostream>
#include<optional>

class Piece
{
public:
	enum class Color : uint8_t
	{
		None,
		Red,
		Yellow,
		Blue,
		Green,
		Orange,
		Purple
	};
	enum class Shape : uint8_t
	{
		None,
		Circle,
		Square,
		Star,
		Diamond,
		Clover,
		Asterisk
	};
public:
	Piece();
	Piece(Color color, Shape shape);
	Piece(const Piece & other);
	Piece(Piece && other);
	~Piece();
public:
	Color GetColor() const;
	Shape GetShape() const;
public:
	Piece & operator = (const Piece & other);
	Piece & operator = (Piece && other);
	bool operator == (const Piece & other) const;
	friend std::istream & operator >> (std::istream & is, Piece & piece);
	friend std::ostream & operator << (std::ostream & os, const Piece & piece);
public:
	bool IsCompatible(const Piece& piece) const;
private:
	Color m_color : 4;
	Shape m_shape : 4;
};
