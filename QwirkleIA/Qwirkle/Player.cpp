#include "Player.h"
#include"Board.h"
#include<random>
#include<string>
#include<fstream>
#include<set>
const int kInvalidPiecePosition = -1;

Player::Player(const std::string & name) : m_name(name)
{
	m_score = 0;
	m_currentPieces.reserve(kInitialBoardSize);
	m_hints = 5;
}

Player::~Player()
{
	m_currentPieces.clear();
}

const std::string & Player::GetName() const
{
	return m_name;
}

const unsigned int Player::GetScore() const
{
	return m_score;
}

std::vector<Piece> Player::GetCurrentPieces() const
{
	return m_currentPieces;
}

uint16_t Player::GetHints() const
{
	return m_hints;
}

void Player::Turn(UnusedPieces & unusedPieces, Board & board, unsigned int opponentScore)
{
	std::ofstream of("syslog.log", std::ios::app);
	Logger logger(of);
	uint16_t option;
	std::cout << "           " << m_name << " is your Turn! (-_-) " << std::endl;
	ShowCurrentPieces();

	std::cout << " The current Board is:" << std::endl;
	std::cout << board;

	Functions::PlayerTurnOptions();
	std::cin >> option;
	switch (option)
	{
	case 1:
	{
		Option1(unusedPieces);
		break;
	}

	case 2:
	{
		Option2(unusedPieces, board, opponentScore);
		break;
	}
	}
}


void Player::PickPiece(Piece & piece)
{
	std::ofstream of("syslog.log", std::ios::app);
	Logger logger(of);

	ShowCurrentPieces();

	std::cin >> piece;
	std::cout << " You want to place the piece: " << piece << std::endl;
	bool correct = false;
	while (!correct)
	{
		try
		{
			correct = PickedCorrectPiece(piece);
		}
		catch (const char* errorMessage)
		{
			std::cout << errorMessage << std::endl;
			std::cout << " Let's Try Again! Pay attention and choose another Piece! " << std::endl;
			std::cin >> piece;
			std::cout << " You want to place the piece: " << piece << std::endl;
			logger.log("Incorrect piece! ", Logger::Level::Error);
		}
	}
}

void Player::PlacePiece(Piece & piece, Board & board, UnusedPieces & unusedPieces, Board::Position& position)
{
	std::ofstream of("syslog.log", std::ios::app);
	Logger logger(of);

	GetPosition(position, piece);
	bool rightPosition = false;

	while (!rightPosition)
	{
		try
		{
			rightPosition = board.RightChoice(position, piece);
		}
		catch (const char* errorMessage)
		{
			std::cout << errorMessage << std::endl;
			std::string option;
			std::cout << " Do you want to quit? Press YES or NO (-_-)" << std::endl;
			std::cin >> option;
			if (option == "YES" || option == "yes")
			{
				break;
				rightPosition = false;
			}
			else
			{
				InsertAnotherPosition(position);
				logger.log("Wrong position!", Logger::Level::Error);
			}
		}
	}
	if (rightPosition)
		board.AddPiece(piece, position);

	VerifyQwirkle(board, position);
	AfterPlacingPiece(piece, board);
}

void Player::DeletePickedPiece(const Piece & piece)
{
	m_currentPieces.erase(m_currentPieces.begin() + FindPiecePosition(piece));
}

void Player::IncreaseScore(const unsigned & newScore)
{
	m_score += newScore;
}

int Player::FindPiecePosition(const Piece & piece)
{
	for (auto position = 0; position < m_currentPieces.size(); ++position)
		if (m_currentPieces[position] == piece)
			return position;
	return kInvalidPiecePosition;
}

void Player::GenerateCurrentPieces(UnusedPieces &Pieces)
{
	std::random_device random_device;
	std::mt19937 engine{ random_device() };

	for (auto index = 0; index < kInitialBoardSize; ++index)
	{
		std::uniform_int_distribution<unsigned> dist(0, Pieces.GetUnusedPieces().size() - 1);
		Piece randomPiece = Pieces.GetUnusedPieces()[dist(engine)];
		m_currentPieces.push_back(randomPiece);
		Pieces.DeletePiece(randomPiece);
	}
}

void Player::Option1(UnusedPieces & unusedPieces)
{
	std::ofstream of("syslog.log", std::ios::app);
	Logger logger(of);
	uint16_t numberOfPieces;
	std::cout << " How many pieces do you want to Change?";
	std::cin >> numberOfPieces;
	logger.Verify(numberOfPieces);
	if (numberOfPieces == kInitialBoardSize)
	{
		ChangeAllPieces(unusedPieces);
		std::cout << " After this Turn, your Pieces are: " << std::endl;
		ShowCurrentPieces();
		std::cout << " Pieces unused: " << unusedPieces.GetUnusedPieces().size() << std::endl;
		logger.log("The player changed all pieces. ", Logger::Level::Info);
	}
	else
	{
		std::cout << " Choose the pieces you want to change:" << std::endl;
		std::vector<Piece> pieces;
		Piece pieceToChange;
		bool correctPiece = false;
		for (auto i = 0; i < numberOfPieces; ++i)
		{
			std::cin >> pieceToChange;
			while (correctPiece == false)
			{
				try
				{
					correctPiece = PickedCorrectPiece(pieceToChange);
				}
				catch (const char* errorMessage)
				{
					std::cout << errorMessage << std::endl;
					std::cout << " Let's Try Again! Pay attention and choose another Piece :) " << std::endl;
					std::cout << " Color + Shape:" << std::endl;
					std::cin >> pieceToChange;
					logger.log("Incorrect piece to change.", Logger::Level::Error);
				}
			}
			pieces.push_back(pieceToChange);
		}
		ChangeSomePieces(pieces, unusedPieces);
		std::cout << " After this Turn, your Pieces are : " << std::endl;
		ShowCurrentPieces();

		logger.log(" The player changed a number of pieces. ", Logger::Level::Info);
		std::cout << " Pieces unused: " << unusedPieces.GetUnusedPieces().size() << std::endl;
	}
}

void Player::Option2(UnusedPieces & unusedPieces, Board & board, const unsigned & opponentScore)
{
	std::ofstream of("syslog.log", std::ios::app);
	Logger logger(of);
	unsigned playerMoves;
	Board::Position position;
	unsigned int lineUp = 0, lineDown = 0, columnLeft = 0, columnRight = 0;
	Piece piece;
	std::vector<Board::Position> positions;

	std::cout << " How many pieces do you want to place to the Board? " << std::endl;
	std::cin >> playerMoves;
	logger.Verify(playerMoves);
	bool placed = false;
	for (unsigned index = 0; index < playerMoves; ++index)
	{
		PickPiece(piece);
		bool insert = true;
		std::string response;

		if (m_score < opponentScore   && m_hints != 0)
		{
			Functions::HintOption();
			std::cin >> response;

			if (response == "y")
			{
				Hint(board, piece, position, index);
				--m_hints;
				placed = true;
				std::cout << m_name, ", now you have ", m_hints, "Hints! Good Luck! (-_-)";
			}
		}
		if (placed == false)
			PlacePiece(piece, board, unusedPieces, position);
		for (auto currentPosition : positions)
			if (position.first == currentPosition.first || position.second == currentPosition.second)
			{
				insert = false;
				break;
			}
		if (insert)
			positions.push_back(position);
		board.SetResize(position, lineUp, lineDown, columnRight, columnLeft);

		std::cout << std::endl;
	}
	GeneratePieces(playerMoves, unusedPieces);
	std::cout << " After this Turn, your pieces are: " << std::endl;
	ShowCurrentPieces();
	std::cout << " After this Turn, your Score is: ";

	IncreaseScore(board.CalculateScore(positions));
	std::cout << m_score << std::endl;
	positions.clear();
	logger.log("The player placed pieces on board.", Logger::Level::Info);
	board.Resize(lineUp, lineDown, columnLeft, columnRight);
	std::cout << std::endl << "Pieces unused: " << unusedPieces.GetUnusedPieces().size() << std::endl;

}


void Player::ChangeAllPieces(UnusedPieces & unusedPieces)
{
	for (auto piece : m_currentPieces)
		unusedPieces.AddPiece(piece);
	m_currentPieces.clear();
	GenerateCurrentPieces(unusedPieces);
}

void Player::ChangeSomePieces(std::vector<Piece> & pieces, UnusedPieces & unusedPieces)
{
	for (auto piece : pieces)
		unusedPieces.AddPiece(piece);
	DeletePieces(pieces);
	GeneratePieces(pieces.size(), unusedPieces);
	pieces.clear();
}

void Player::InsertAnotherPosition(Board::Position & position)
{
	unsigned line, column;
	std::cout << " Please insert another position!" << std::endl;
	std::cout << " Line : "; std::cin >> line;
	std::cout << " Column : "; std::cin >> column;
	position.first = line - 1;
	position.second = column - 1;
}

void Player::GetPosition(Board::Position & position, const Piece & piece)
{
	unsigned int line, column;

	std::cout << " " << m_name << ", please select the location for Piece " << piece << std::endl;
	std::cout << " Line: "; std::cin >> line;
	std::cout << " Column: "; std::cin >> column;
	position.first = line - 1;
	position.second = column - 1;
}

void Player::AfterPlacingPiece(const Piece & piece, const Board & board)
{
	DeletePickedPiece(piece);
	std::cout << std::endl;
	ShowCurrentPieces();
	std::cout << board;
}

void Player::VerifyQwirkle(const Board & board, const Board::Position & position)
{
	std::ofstream of("syslog.log", std::ios::app);
	Logger logger(of);
	if (board.IsQwirkle(position))
	{
		std::cout << "QWIRKLE!" << std::endl;
		IncreaseScore(6);
		logger.log("QWIRKLE!", Logger::Level::Warning);
	}
}

void Player::Hint(Board & board, const Piece & piece, Board::Position & position, unsigned & move)
{
	std::ofstream of("syslog.log", std::ios::app);
	Logger logger(of);

	unsigned maxScore = 0;
	Board::Position bestPosition;
	BoardPieces boardPieces(board);

	std::vector<std::pair<Piece, Board::Position>> compatiblesPiecesWithPositions = boardPieces.FindCompatiblePiecesWithPositions(piece);
	if (!compatiblesPiecesWithPositions.empty())
	{
		for (auto pairOfPieceCompatibleAndPosition : compatiblesPiecesWithPositions)
		{
			std::vector<Board::Position>  foundPositions = boardPieces.CheckPositions(board, pairOfPieceCompatibleAndPosition.first, pairOfPieceCompatibleAndPosition.second);
			for (auto currentPosition : foundPositions)
			{
				try
				{
					if (board.RightCompatibility(piece, currentPosition))
					{
						board.AddPiece(piece, currentPosition);
						unsigned currentScore = (board.CalculateParticularScore(currentPosition));
						if (currentScore > maxScore)
						{
							maxScore = currentScore;
							bestPosition = currentPosition;
						}
						board.DeletePiece(piece, currentPosition);
					}
				}
				catch (const char* errorMessage)
				{
					logger.log(errorMessage, Logger::Level::Info);
				}
			}
		}
		position.first = bestPosition.first;
		position.second = bestPosition.second;
		board.AddPiece(piece, position);
		VerifyQwirkle(board, position);
		AfterPlacingPiece(piece, board);
	}
	else
	{
		std::cout << "Piece can not be placed! There are no compatible Pieces on the Board!";
		--move;
		++m_hints;
	}

}



void Player::DeletePieces(std::vector<Piece> & pieces)
{
	for (auto piece : pieces)
		DeletePickedPiece(piece);
}

void Player::ShowCurrentPieces()
{
	std::cout << m_name << ", your Current Pieces are: " << std::endl;
	for_each(m_currentPieces.begin(), m_currentPieces.end(), [](const Piece & piece) {std::cout << "   " << piece << std::endl; });
	std::cout << std::endl;
}


void Player::GeneratePieces(const int & number, UnusedPieces & unusedPieces)
{
	std::random_device random_device;
	std::mt19937 engine{ random_device() };

	for (auto index = 0; index < number; ++index)
	{
		std::uniform_int_distribution<unsigned> dist(0, unusedPieces.GetUnusedPieces().size() - 1);
		Piece randomPiece = unusedPieces.GetUnusedPieces()[dist(engine)];
		m_currentPieces.push_back(randomPiece);
		unusedPieces.DeletePiece(randomPiece);
	}
}

bool Player::PickedCorrectPiece(const Piece & piece) const
{
	int color = static_cast<int>(piece.GetColor()) - 1;
	int shape = static_cast<int>(piece.GetShape()) - 1;
	if (color < 0 || color> 5)
	{
		throw (" You inserted wrong Color attribute for the Piece! ");
		return false;
	}
	if (shape < 0 || shape >5)
	{
		throw (" You inserted wrong Shape attribute for the Piece! ");
		return false;
	}

	for (Piece currentPiece : m_currentPieces)
		if (currentPiece == piece)
			return true;
	throw(" You selected a piece that doesn't exist in your collection. :( ");
	return false;
}
