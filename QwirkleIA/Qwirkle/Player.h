#pragma once
#include<iostream>
#include<set>
#include"Piece.h"
#include"UnusedPieces.h"
#include"Board.h"
#include"BoardPieces.h"
#include"../QwirkleDll/QwirkleDll.h"
#include<iterator>

class Player
{
public:
	Player() = default;
	Player(const std::string & name);
	~Player();
public:
	const std::string& GetName() const;
	const unsigned int GetScore() const;
	std::vector<Piece> GetCurrentPieces() const;
	uint16_t GetHints() const;
public:
	void IncreaseScore(const unsigned& newScore);
public:
	void Turn(UnusedPieces & unusedPieces, Board & board, unsigned int opponentScore);
	void PickPiece(Piece & piece);
	void PlacePiece(Piece & piece, Board & board, UnusedPieces & unusedPieces, Board::Position & position);
public:
	void DeletePickedPiece(const Piece & piece);
	void DeletePieces(std::vector<Piece> & pieces);
public:
	void GeneratePieces(const int & number, UnusedPieces & unusedPieces);
	void GenerateCurrentPieces(UnusedPieces & Pieces);
public:
	void Option1(UnusedPieces & unusedPieces);
	void Option2(UnusedPieces & unusedPieces, Board & board, const unsigned & opponentScore);
public:
	void ShowCurrentPieces();
public:
	int FindPiecePosition(const Piece & piece);
	bool PickedCorrectPiece(const Piece & piece) const;
public:
	void ChangeAllPieces(UnusedPieces & unusedPieces);
	void ChangeSomePieces(std::vector<Piece> & pieces, UnusedPieces & unusedPieces);
public:
	void InsertAnotherPosition(Board::Position & position);
	void GetPosition(Board::Position & position, const Piece& piece);
public:
	void AfterPlacingPiece(const Piece & piece, const Board & board);
	void VerifyQwirkle(const Board & board, const Board::Position & position);
public:
	void Hint(Board & board, const Piece & piece, Board::Position & position, unsigned & move);
private:
	std::string m_name;
	unsigned int m_score;
	std::vector<Piece> m_currentPieces;
	uint16_t m_hints;
};
