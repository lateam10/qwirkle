#pragma once
#include"Board.h"
#include"Player.h"
#include"../QwirkleDll/QwirkleDll.h"

class QwirkleGame
{
public:
	void Run();
	void ChooseNumberOfPieces(Player& player, unsigned int & playerMoves);
	void Clear();
};

