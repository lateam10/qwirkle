#include "UnusedPieces.h"
#include"Board.h"
#include<algorithm>
#include<iostream>
#include<vector>
#include<random>


UnusedPieces::UnusedPieces()
{
	GenerateUnusedPieces();
}


UnusedPieces::~UnusedPieces()
{
	pieces.clear();
}

void UnusedPieces::GenerateUnusedPieces()
{
	std::random_device randomDevice;
	std::mt19937 generator(randomDevice());
	int count = 0;
	do
	{
		for (auto index1 = 0; index1 < kInitialBoardSize; ++index1)
			for (auto index2 = 0; index2 < kInitialBoardSize; ++index2)
				pieces.push_back(Piece(static_cast<Piece::Color>(index1 + 1), static_cast<Piece::Shape>(index2 + 1)));
		count++;
	} while (count < kCombinations);
	std::shuffle(pieces.begin(), pieces.end(), generator);
}

std::vector<Piece> UnusedPieces::GetUnusedPieces() const
{
	return pieces;
}

void UnusedPieces::AddPiece(const Piece & piece)
{
	pieces.push_back(piece);
}

void UnusedPieces::DeletePiece(const Piece & piece)
{
	auto it = std::find(pieces.begin(), pieces.end(), piece);
	if (it != pieces.end())
	{
		pieces.erase(pieces.begin() + std::distance(pieces.begin(), it));
	}
}

void UnusedPieces::DeletePieces(const std::vector<Piece> & pieces)
{
	std::for_each(pieces.begin(), pieces.end(), [&](const Piece& piece) {
		DeletePiece(piece);
	});

}

std::ostream & operator<<(std::ostream & os, const UnusedPieces& pieces)
{
	const auto& unusedPieces = pieces.GetUnusedPieces();
	std::for_each(unusedPieces.begin(), unusedPieces.end(), [&](const Piece& piece) {
		os << piece << " ";
	});
	return os;
}
