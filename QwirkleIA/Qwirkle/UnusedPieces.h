#pragma once
#include"Piece.h"
#include<vector>

const unsigned kCombinations = 3;

class UnusedPieces
{
public:
	UnusedPieces();
	~UnusedPieces();
public:
	std::vector<Piece> GetUnusedPieces() const;
public:
	void GenerateUnusedPieces();
public:
	void AddPiece(const Piece & piece);
	void DeletePiece(const Piece & piece);
	void DeletePieces(const std::vector<Piece> & pieces);
public:
	friend std::ostream & operator << (std::ostream & os, const UnusedPieces & pieces);
private:
	std::vector<Piece> pieces;
};

