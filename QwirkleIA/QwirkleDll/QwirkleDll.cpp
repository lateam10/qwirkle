#include "QwirkleDll.h"

const char* LogLevelToString(Logger::Level level)
{
	switch (level)
	{
	case Logger::Level::Info:
		return "Info";
	case Logger::Level::Warning:
		return "Warning";
	case Logger::Level::Error:
		return "Error";
	default:
		return "";
	}
}

Logger::Logger(std::ostream & os, Logger::Level minimumLevel) :
	os{ os },
	minimumLevel{ minimumLevel }
{
	
}

void Logger::log(const char * message, Level level)
{
	if (static_cast<int>(level) < static_cast<int>(minimumLevel))
		return;

	os << "[" << LogLevelToString(level) << "] " << message << std::endl;
}

void Logger::log(const std::string & message, Level level)
{
	this->log(message.c_str(), level);
}

void Logger::SetMinimumLogLevel(Level level)
{
	this->minimumLevel = level;
}

void Logger::Verify(int a)
{
	while (a > 6 && a < 1)
	{
		std::cout << " The number is wrong. Please try again a number from 1 to 6." << std::endl;
		std::cin >> a;
		this->log("Wrong number.", Logger::Level::Error);
	}
}

void Logger::VerifyOption(int b)
{
	while (b != 1 && b != 2) {
		std::cout << " The number of your option is not valid. Please try again." << std::endl;
		std::cin >> b;
		this->log("Wrong options's number.", Logger::Level::Error);
	}
}

void Functions::Introduction()
{

	std::cout << std::endl << std::endl;
	std::cout << "  Some things about Qwirkle: " << std::endl;
	std::cout << "  -> You have 108 pieces in 6 colors and with 6 different shapes." << std::endl << std::endl;
	std::cout << "  -> You receive 6 random pieces. On your turn you can do one of the 3 actions: " << std::endl;
	std::cout << "		 # Add one piece to the board and extract a piece to bring your deck again to 6." << std::endl;
	std::cout << "		 # Add two or more pieces to the board." << std::endl
		<< "		   All pieces played must share one attribute (color or shape) and must be placed in the same line." << std::endl
		<< "		   Again, extract pieces until your deck has 6 pieces again."
		<< std::endl;
	std::cout << "		 # Change between 1-6 pieces with another random pieces." << std::endl << std::endl;
	std::cout << "  -> Each player has 5 hints to help with the optimal placement of a piece on the board. " << std::endl;
	std::cout << std::endl << std::endl;

	system("pause");
	system("cls");

	std::cout << std::endl << std::endl;
	std::cout << " !!! REMEMBER (-_-)" << std::endl;
	std::cout << "  When you want to place a Piece on the Board, you should enter, by turn, the LINE and the COLUMN.";
	std::cout << std::endl << std::endl << std::endl;
	std::cout << "          Now just take a coffee and pay attention! You could get a challenge if you lose!" << std::endl << std::endl;
	system("pause");
	system("cls");
}

void Functions::PlayerTurnOptions()
{

	std::cout << " Your options are: " << std::endl;
	std::cout << " 1. To change one or more pieces" << std::endl;
	std::cout << " 2. To put Pieces on the Board" << std::endl;
	std::cout << " Enter the number of your option: ";
}

void Functions::HintOption()
{
	std::cout << " Your Score is lower than your opponent. Do you want to use one of your Hints?" << std::endl;
	std::cout << "Enter : " << std::endl;
	std::cout << "-> 'y' (YES) " << std::endl;
	std::cout << "-> 'n' (NO) " << std::endl;
}

