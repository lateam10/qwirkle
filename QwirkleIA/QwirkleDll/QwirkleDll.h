#pragma once

#ifdef QWIRKLEDLL_EXPORTS
#define QWIRKLEDLL_API __declspec(dllexport)
#else
#define QWIRKLEDLL_API __declspec(dllimport)
#endif 

#include <iostream>
#include <string>
#include<vector>
#include<exception>
#include<fstream>


class QWIRKLEDLL_API Functions
{
public:
	static void Introduction();
	static void PlayerTurnOptions();
	static void HintOption();
};


class QWIRKLEDLL_API Logger
{
public:
	enum class Level
	{
		Info,
		Warning,
		Error
	};
public:
	Logger(std::ostream& os, Level minimumLevel = Level::Info);
public:
	void log(const char * message, Level level);
	void log(const std::string& message, Level level);
public:
	void SetMinimumLogLevel(Level level);
public:
	void Verify(int a);
	void VerifyOption(int b);
private:
	std::ostream& os;
	Level minimumLevel;
};