#include "stdafx.h"
#include "CppUnitTest.h"

#include"Board.h"
#include"Board.cpp"

#include<iostream>
#include<memory>


using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace QwirkleTests
{
	TEST_CLASS(BoardTests)
	{
	public:
		TEST_METHOD(DefaultConstructor)
		{
			Board board;
			Assert::IsFalse(board.GetBoard().size() != kInitialBoardSize);
		}
		TEST_METHOD(DefaultConstructorEmptyBoard)
		{
			Board board;
			if (!board.IsEmpty())
				Assert::Fail();
		}

		TEST_METHOD(SetGetAtOneOne)
		{
			Board board;
			Piece piece;
			Board::Position position{ 1,1 };
			board.AddPiece(piece, position);

			Assert::IsFalse(board.IsEmpty());
		}
		TEST_METHOD(AddUpLine)
		{
			Board board;
			board.AddUpLine();
			Board::Position position{ 0,0 };
			Assert::IsTrue(board[position]==std::nullopt);
		}
		TEST_METHOD(GetAtOneOne)
		{
			Board board;
			Board::Position position{ 1,1 };
			Piece piece(Piece::Color::Blue, Piece::Shape::Circle);
			board.AddPiece(piece, position);
			Assert::IsTrue(board[position].has_value());
		}

		TEST_METHOD(GetAtMinusOneOne)
		{
			Board board;
			Board::Position position{ -1, 1 };

			Assert::ExpectException<const char*>
				([&] () { board[position] ;});
		}
		
		TEST_METHOD(GetAtOneMinusOne)

		{
			Board board;
			Board::Position position{ 1,-1 };
			Assert::ExpectException<const char*>
				([&]() { board[position]; });
		}
		TEST_METHOD(DuplicatePiece)
		{
			Board board;
			Board::Position position1{ 1,1 };
			Board::Position position2{ 1,2 };
			Board::Position position3{ 1,3 };
			Piece piece1(Piece::Color::Red, Piece::Shape::Circle);
			Piece piece2(Piece::Color::Red, Piece::Shape::Square);
			Piece piece3(Piece::Color::Red, Piece::Shape::Circle);
			board.AddPiece(piece1, position1);
			board.AddPiece(piece2, position2);
			board.AddPiece(piece3, position3);
			Assert::IsTrue(board.DuplicatePiece(piece3,position3));
		}
		TEST_METHOD(IzolatePiece)
		{
			Board board;
			Board::Position position1{ 1,1 };
			Board::Position position2{ 1,3 };
		
			Piece piece1(Piece::Color::Red, Piece::Shape::Circle);
			Piece piece2(Piece::Color::Red, Piece::Shape::Square);
			
			board.AddPiece(piece1, position1);
			board.AddPiece(piece2, position2);
			
			Assert::IsTrue(board.IzolatePiece(position2));
		}
		TEST_METHOD(IzolatePieceSpecialCaseEmptyBoard)
		{
			Board board;
			Board::Position position1{ 1,1 };
			Assert::IsFalse(board.IzolatePiece(position1));
		}
		TEST_METHOD(RightChoice)
		{
			Board board;
			Board::Position position1{ 2,2 };
			Board::Position position2{ 2,3 };
			Board::Position position3{ 3,2 };

			Piece piece1(Piece::Color::Green, Piece::Shape::Asterisk);
			Piece piece2(Piece::Color::Orange, Piece::Shape::Asterisk);
			Piece piece3(Piece::Color::Orange, Piece::Shape::Diamond);
			Assert::IsTrue(board.RightChoice(position3,piece3));
		}
		TEST_METHOD(VerifyQwirkle)
		{
			Board board;
			Board::Position position1{ 1,0 };
			Board::Position position2{ 1,1 };
			Board::Position position3{ 1,2 };
			Board::Position position4{ 1,3 };
			Board::Position position5{ 1,4 };
			Board::Position position6{ 1,5 };
			Piece piece1(Piece::Color::Blue, Piece::Shape::Circle);
			Piece piece2(Piece::Color::Red, Piece::Shape::Circle);
			Piece piece3(Piece::Color::Green, Piece::Shape::Circle);
			Piece piece4(Piece::Color::Orange, Piece::Shape::Circle);
			Piece piece5(Piece::Color::Purple, Piece::Shape::Circle);
			Piece piece6(Piece::Color::Yellow, Piece::Shape::Circle);
			board.AddPiece(piece1, position1);
			board.AddPiece(piece2, position2);
			board.AddPiece(piece3, position3);
			board.AddPiece(piece4, position4);
			board.AddPiece(piece5, position5);
			board.AddPiece(piece6, position6);
			Assert::IsTrue(board.IsQwirkle(position1));
		}

		TEST_METHOD(HasCorrectPiece)
		{
			Board board;
			Piece piece(Piece::Color::Red, Piece::Shape::Star);
			Board::Position position{ 3,3 };

			Assert::IsTrue(board.RightCompatibility(piece, position));
		}

		TEST_METHOD(VerifyBoardUnique)
		{
			//Board board;
			std::unique_ptr<uint8_t> unique = std::make_unique<uint8_t>();
			std::unique_ptr<Board> uPtr = std::make_unique<Board>();

			Assert::IsTrue(unique.get()&&uPtr.get());
		}

	};
}