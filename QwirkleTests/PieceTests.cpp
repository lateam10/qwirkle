#include "stdafx.h"
#include "CppUnitTest.h"

#include"Piece.h"
#include"Piece.cpp"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace QwirkleTests
{		
	TEST_CLASS(PieceTests)
	{
	public:
		
		TEST_METHOD(DefaultConstructor)
		{
			Piece piece(Piece::Color::Blue, Piece::Shape::Circle);

			Assert::IsTrue(Piece::Color::Blue == piece.GetColor());
			Assert::IsTrue(Piece::Shape::Circle== piece.GetShape());
		}

		TEST_METHOD(PieceEquality)
		{
			Piece piece1(Piece::Color::Orange, Piece::Shape::Asterisk);
			Piece piece2(Piece::Color::Green, Piece::Shape::Clover);

			Assert::IsFalse(piece1 == piece2);
		}

		TEST_METHOD(PieceCompatibility)
		{
			Piece piece1(Piece::Color::Blue, Piece::Shape::Circle);
			Piece piece2(Piece::Color::Blue, Piece::Shape::Diamond);
			Assert::IsTrue(piece1.IsCompatible(piece2));
		}
		TEST_METHOD(PieceCompatibilityInCaseOfEquality)
		{
			Piece piece1(Piece::Color::Red, Piece::Shape::Square);
			Piece piece2(Piece::Color::Red, Piece::Shape::Square);
			Assert::IsFalse(piece1.IsCompatible(piece2));
		}
		TEST_METHOD(VerifyPieceUnique)
		{
			std::unique_ptr<uint8_t> unique = std::make_unique<uint8_t>();
			std::unique_ptr<Piece> uPtr = std::make_unique<Piece>(Piece::Color::Blue,Piece::Shape::Asterisk);

			Assert::IsTrue(unique.get()&&uPtr.get());
		}

	};
}