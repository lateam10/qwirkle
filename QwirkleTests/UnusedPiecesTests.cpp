#include "stdafx.h"
#include "CppUnitTest.h"

#include"UnusedPieces.h"
#include"UnusedPieces.cpp"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace QwirkleTests
{
	TEST_CLASS(UnusedPiecesTests)
	{
	public:

		TEST_METHOD(DefaultConstructor)
		{
			UnusedPieces unusedPieces;
			Assert::IsTrue(unusedPieces.GetUnusedPieces().size() == 108);
		}
	};
}